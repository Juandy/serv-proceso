#include <stdio.h>
#include <sys/shm.h>
#include <sys/stat.h>

int main ()
{
    int segment_id;
    char* shared_memory;
    struct shmid_ds shmbuffer;
    int segment_size;
    const int shared_segment_size = 0x6400;

    segment_id = shmget (IPC_PRIVATE, shared_segment_size,
                        IPC_CREAT | IPC_EXCL | IPC_IRUSR | S_IWUSR);
    shared_memory = (char*) shmat (segment_id, 0, 0);
    printf ("Memoria compartida adjuntada a la direccion %p\n", shared_memory);
    shmctl (segment_id, IPC_STAT, &shmbuffer); //Determina el tamaño del segmento
    segment_size = shmbuffer.shm_segsz;
    printf ("Tamaño del segmento: %d\n", segment_size);
    sprintf (shared_memory, "Hello, wold."); // Escribimos una cadena en el segmento de memoria compartida
    shmdt (shared_memory); //
}
