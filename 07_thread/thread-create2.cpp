#include <pthread.h>
#include <stdio.h>

//Parametro para pintar la funcion

struct char_print_parms
{
    char character; // caracter a pintar
    int count;//Veces a pintar
};

void* char_print (void* parameters)
{
    struct char_print_parms* p = (struct char_print_parms*) parameters;
    int i;

    for(i=0; i< p->count; i++)
        fputc (p->character,stderr);
    return NULL;
}

//Programa principal

int main()
{
    pthread_t thread1_id;
    pthread_t thread2_id;

    struct char_print_parms thread1_args;
    struct char_print_parms thread2_args;

    //Creamos nuevo hilo que pintara 30,000 veces x
    thread1_args.character = 'X';
    thread1_args.count = 30;
    pthread_create (&thread1_id, NULL, &char_print,&thread1_args);

    //Creamos nuevo hilo que pintara 30,000 veces x
    thread2_args.character = 'O';
    thread2_args.count = 30;
    pthread_create (&thread2_id, NULL, &char_print,&thread2_args);

    pthread_join (thread1_id,NULL);
    pthread_join (thread2_id,NULL);

    return 0;

}
