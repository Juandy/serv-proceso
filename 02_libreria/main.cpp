#include <stdio.h>
#include <stdlib.h>
#include "aritmetica.h"

void resultadoFinal (double res) {
  printf ("El resultado final es: %lf \n", res);
}

void Op (int op, double n1, double n2)
{
  double res;
  if (op == 1)
      res = suma (n1, n2);
  else
      res = resta (n1, n2);

  resultadoFinal (res);
}

int main (int argc, char *argv[])
{
    double n1,
           n2;
    int op;

    printf (" Introduce el primer numero: ");
    scanf  (" %lf", &n1);
    printf (" Introduce el segundo numero: ");
    scanf  (" %lf", &n2);

    printf (" ¿Deseas Sumar (1) o Restar (2) ?: ");
    scanf  (" %i", &op);

    Op(op, n1, n2);

    return EXIT_SUCCESS;
}
