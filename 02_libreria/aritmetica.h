#ifndef __ARITMETICA_H_
#define __ARITMETICA_H_

#ifdef __CPLUSPLUS
extern "C"
{
  #endif
    double suma (double n1, double n2);
    double resta (double n1, double n2);

#ifdef  __CPLUSPLUS

}
#endif

#endif

