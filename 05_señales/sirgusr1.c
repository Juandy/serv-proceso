#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

sig_atomic_t sigusr1_count = 0;

void handler (int signal_numbre)
{
    ++sigusr1_count;
}

int main()
{
    int n;
    struct sigaction sa;
    memset (&sa, 0, sizeof(sa));
    sa.sa_handler = &handler;
    sigaction (SIGUSR1, &sa, NULL);

    scanf("%i",&n);

    printf ("SIGUSR1 es repetido : %i veces \n", sigusr1_count);

    return 0;
}
