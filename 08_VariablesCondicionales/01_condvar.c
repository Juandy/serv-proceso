//gcc -o mainCondvar 01_condvar.c -lpthread
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;

void initialize_flag()
{
    /*inicializamos el mutex y la variable condicionada*/
    pthread_mutex_init (&thread_flag_mutex, NULL);
    pthread_cond_init  (&thread_flag_cv, NULL);
    /*iniciamos el valor de flag*/
    thread_flag = 0;
}
void do_work()
{
    printf("Hola mundo\n\n");
}
/*Llamamos repetidamente a do_work hasta que tenga un valor, si sigue vacia se bloqueara*/
void* thread_function (void* thread_arg)
{
    while(1)
    {
        pthread_mutex_lock (&thread_flag_mutex);      /*Bloqueamos la variable mutex antes de acceder al valor*/
        while(!thread_flag)
            /*La variable "flag" esta limpia, esperando  por una señal en la  variable condicional
             * indicando que el valor de "flag", cuando la señal llega y el hilo esta bloqueado,hace
             * de nuevo el bucle y revisa flag de nuevo*/
            pthread_cond_wait (&thread_flag_cv, &thread_flag_mutex);
        /*Cuando llegamos aqui sabemos el valor de "Flag" y el mutex se desbloquea*/
        pthread_mutex_unlock  (&thread_flag_mutex);
        do_work ();
    }
    return NULL;
}
/*Establece el valor de la bandera del hilo en FLAG_VALUE.*/
void set_thread_flag (int flag_value)
{
    /*Bloquea mutex antes de acceder al valor de "flag"*/
    pthread_mutex_lock (&thread_flag_mutex);
    /*Establece el valor de "flag" y la señal en caso de que "thread_function se bloquee"*/
    thread_flag = flag_value;
    pthread_cond_signal (&thread_flag_cv);
    /*Desbloquemaos mutex*/
    pthread_mutex_unlock (&thread_flag_mutex);
}

int main ()
{
    pthread_t thread_id;
    initialize_flag();

    set_thread_flag(2);

    pthread_create (&thread_id, NULL, &thread_function, NULL);

    pthread_join (thread_id, NULL);

    return 0;
}
