#ifndef __LIB2_H__
#define __LIB2_H__


#ifdef __cplusplus
extern "C" {
#endif

    const char **catalogo();
    int multi (int op1, int op2);
    int divi (int op1, int op2);

#ifdef __cplusplus
}
#endif



#endif

