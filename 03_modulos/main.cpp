#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include "lib1.h"
#include "lib2.h"

    char *progname

        struct TLista lista{
            char **data;
            int summit;
        };

    void push (const char *fn_name){
        lista.data = (char **) realloc ( lista.data, (lista.summit + 1) * sizeof (char **));
        lista.data[lista.summit++] = strdup (fn_name);
    }

    void print_usage (FILE *out){
        fprintf (out,
        "\n"
        "\n%s <config_file>\t - Loads modules from config file and prints function names contained within."
        "\n\n", progname);
    }

    void show_functions() {
        printf ("REGISTERED FUNCTIONS\n");
        printf ("====================\n");
        printf ("\n");
            for (int i=0; i<lista.summit; i++)
                printf ("%s\n", lista.data[i]);
        printf ("\n");
    }
    void deliver() {
        for (int i=0; i<lista.summit; i++)
            free (lista.data[i]);
        free (lista.data);
    }

    int main (int argc, char *argv[]) {
        void *handle = NULL;
        FILE *module = NULL;
        char modname[0x100];

        progname = argv[0];

        if (argc<2)
            die("Not enough arguments.", 1);

        if ( !(module = fopen(argv[1], "r") ))
            die ("Config file not found.", 2);

        while ( !feof(module) )
            if (fscanf (module," %s", modname) != EOF){
                 DEBUG ( "Opened %s\n", modname );
                 handle = dlopen (modname, RTLD_LAZY);

                  if (handle) {
                     heed_the_call(handle);
                     dlclose (handle);
                   } else
                      fprintf (stderr, "Couldn't find module: %s\n", modname);
            }

        show_functions ();
        deliver ();

        return 0;
}
