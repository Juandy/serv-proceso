#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main (int argc, char *argv[])
{
    int n1, n2;

    n1 = atoi (argv[1]);
    n2 = atoi (argv[2]);

    int resultado = n1 + n2;

    return resultado;
}
