#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


int spawn (char* program, char** arg_list)
{
    pid_t child_pid;                    //pid_t es igual a un int
    child_pid = fork ();                //El fork divide el programa en 2, siendo el padre y el hijo iguales exepto en el return

    //Comprobamos si el padre devuelve un valor
    if(child_pid !=0)
        return child_pid;
    else
    {   //Se ejecuta el programa hijo
        execvp (program, arg_list);
        fprintf (stderr, "Un error ocurrio en execvp\n");
        abort ();
    }
}

int main ()
{
    int op;

    int child_status;
    char* arg_list[] = {"./suma","3","7", NULL};   //Lista de parametros
    char* arg_list1[] = {"./resta","7","3", NULL};   //Lista de parametros

    printf ("1 para sumar 2 para restar: ");
    scanf  ("%i",&op);

    if(op == 1)
        spawn ("./suma", arg_list);
    else if(op == 2)
        spawn ("./resta", arg_list1);

    wait(&child_status);

    if(WIFEXITED (child_status) )
        printf ("El proceso hijo a salido bien, la suma es: %d\n", WEXITSTATUS(child_status) );
    else
        printf ("El proceso hijo no ha salido bien\n");

    printf ("Buen dia \n");

    return 0;
}
