/*****************************************************************************************************************************************************/
//gcc -o hijo hijo.cpp
//
//By Juan Diego Fernandez
//Asignatura Servicio y procesos
//
//Description
//Programa que creara dos procesos simultaneos padre e hijo y se le pasara señales, en concreto 3 para poder acabar con el hijo
//Este es el programa hijo
//
//Version 1.0  23 Noviembre 2019
/*****************************************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define DELAY 2

sig_atomic_t sigusr1_count = 0;

/*miramos las señales que recibe y las cuenta*/
void handler (int signal_numbre)
{
    ++sigusr1_count;
}

int main()
{
    int n;
    struct sigaction sa;
    memset (&sa, 0, sizeof(sa));
    sa.sa_handler = &handler;
    sigaction (SIGUSR1, &sa, NULL);


    /*Finalizara cuando le llegen 3 SIGUSR1*/
    while(sigusr1_count < 3){
		/*Dormimos el programa para dar tiempo a las señales*/
        sleep(DELAY);
		/*Comprobante de si las señales llegan bn*/
        printf("SIGUSR1 %i\n", sigusr1_count);
    }

    printf ("\nSIGUSR1 es repetido : %i veces \n", sigusr1_count);

    return 0;
}

