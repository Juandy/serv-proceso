/*****************************************************************************************************************************************************/
// gcc -o obreros obreros.cpp -lpthread
//
//By Juan Diego Fernandez
//Asignatura Servicio y procesos
//
//Description
//Programa que controla mediante semaforos e hilos el flujo de ladrillos que llevan los obreros (Benito y Mariano)
//si no hay ladrillos se notificara que no hay mas y si se llega al tome de fabricacion(600) la fabrica dejara de hacer ladrillos
//
//Version 1.0  25 Noviembre 2019
/*****************************************************************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

/*Numero de ladrillos maximos en fabrica , tiempo se siesta de los trabajadores y variable global que incia los atributos*/
#define MAX_LADRILLOS_F 50
#define TIME_SIESTA 1
#define INIT_VALOR_GLOBAL 0

/*Creacion e inicializacion de la variable mutex*/
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/*Ladrillos cogidos, los que cogera cada trabajador, el total de ladrillos en almacen, ladrillos en fabrica*/
int ladrillosCogidos =  INIT_VALOR_GLOBAL;
int ladrillosPerVez = INIT_VALOR_GLOBAL;
int ladrillosAlmacenados = MAX_LADRILLOS_F;
int ladrillosFabrica = INIT_VALOR_GLOBAL;

/*Variable semaforo para el control de los ladrillos*/
sem_t sem_id;

/*ladrillos que llevaran los obreros el numero es un random*/
void LadrillosLlevados(){

    int total = 0;

    total = rand ();

    if(total % 2 == 0)
        ladrillosPerVez = 1;
    else{
        if(ladrillosFabrica == MAX_LADRILLOS_F - 1)
            ladrillosPerVez = 1;
        else
            ladrillosPerVez = 2;
    }

}

/*Trabajo del hilo Mariano*/
void* ObreroMariano (void* args) {

    while(ladrillosCogidos < MAX_LADRILLOS_F){
        sem_wait (&sem_id);
        pthread_mutex_lock (&mutex);


        if(ladrillosFabrica < MAX_LADRILLOS_F){
            LadrillosLlevados();
            printf("Mariano lleva %i ladrillos en mano\n", ladrillosPerVez);
            ladrillosFabrica += ladrillosPerVez;
            ladrillosCogidos += ladrillosPerVez;
            printf("ladrillos en fabrica %i\n", ladrillosFabrica);
        }

        pthread_mutex_unlock (&mutex);
        sem_post (&sem_id);

	/*Informamos si hay ladrillos o no para no crear un interbloqueo pasivo*/
        if(ladrillosFabrica < MAX_LADRILLOS_F)
            printf("Mariano se va a dormir turno de Benito.\n\n");
        else
            printf("Mariano llevo el ultimo ladrillo no hay mas.\n\n");
        sleep(TIME_SIESTA);
    }

    return NULL;
}

/*Trabajo del hilo Benito*/
void* ObreroBenito (void* args) {

    while(ladrillosCogidos < MAX_LADRILLOS_F){
        sem_wait (&sem_id);
        pthread_mutex_lock (&mutex);


        if(ladrillosFabrica < MAX_LADRILLOS_F){
            LadrillosLlevados();
            printf("Benitolleva %i ladrillos en mano\n", ladrillosPerVez);
            ladrillosFabrica += ladrillosPerVez;
            ladrillosCogidos += ladrillosPerVez;
            printf("ladrillos en fabrica %i\n", ladrillosFabrica);
        }

        pthread_mutex_unlock (&mutex);
        sem_post (&sem_id);

        if(ladrillosFabrica < MAX_LADRILLOS_F)
            printf("Benito se va a dormir turno de Mariano.\n\n");
        else
            printf("Benito llevo el ultimo ladrillo no hay mas.\n\n");
        sleep(TIME_SIESTA);

    }

    return NULL;
}

int main (int argc, char* argv[]) {

	/*Declaracion de los hilos que se usaran 1 por trabajador*/
    pthread_t Benito;
	pthread_t Mariano;

	/*semilla para el random de ladrillos*/
    srand (time (NULL));
	/*Iniciacion del semaforo*/
    sem_init (&sem_id, 0, 1);

	/*Creacion de los hilos*/
    pthread_create (&Benito, NULL, &ObreroBenito, &sem_id);
    pthread_create (&Mariano, NULL, &ObreroMariano, &sem_id);

	/*Fin de los hilos*/
    pthread_join (Benito, NULL);
	printf ("Benito acabo;\n");
    pthread_join (Mariano, NULL);
	printf ("Mariano acabo;\n");
	/*Destruccion de los semaforos*/
    sem_destroy (&sem_id);

    return EXIT_SUCCESS;
}
