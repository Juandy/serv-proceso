/*****************************************************************************************************************************************************/
//gcc -o padre padre.cpp
//
//By Juan Diego Fernandez
//Asignatura Servicio y procesos
//
//Description
//Programa que creara dos procesos simultaneos padre e hijo y se le pasara señales, en concreto 3 para poder acabar con el hijo
//
//Version 1.0  23 Noviembre 2019
/*****************************************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

/*El programa se queda dormido 1 segundo para dar tiempo a las señales, programa hijo corriendo paralelamente,
/veces que hay que mandar la señal*/
#define DELAY 1
#define SON_PROGRAM "./hijo"
#define V_SIGNAL 3

int spawn (char* program, char** arg_list)
{
    pid_t child;
    child = fork ();

    //Comprobamos si el padre devuelve un valor
    if(child !=0){
        return child;
    }
    else
    {
        execvp (program, arg_list);
        fprintf (stderr, "Un error ocurrio en execvp\n");
        abort ();
    }
}

int main ()
{
    pid_t pid_child; //Guardamos el pid del hijo

    int child_status;
    char* arg_list[] = {(char *) SON_PROGRAM, NULL};

    pid_child = spawn ((char *) SON_PROGRAM, arg_list); //Llamamos a la funcion spawn y nos devuelve el valor del hijo

    int i = 0;

    /*Envio de las señales*/
    while(i < V_SIGNAL){
        sleep(DELAY);
        i++;
		/*Enviamos la señal SIGUSR1 al programa hijo para posteriormete "matarlo"*/
        kill(pid_child, SIGUSR1);
		 /*Comprobande de las señales, si llegan correctamente*/
        printf("Enviadas: %i señales\n", i);
    }
	/*Comprobamos el estatus del hijo, su estado*/
    wait(&child_status);

    if(WIFEXITED (child_status) )
        printf ("El hijo a salido Correctamente\n");
    else
        printf ("El hijo no ha salido como planeaba\n");

    return 0;
}

